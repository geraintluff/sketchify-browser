var CanvasSketch = (function () {
	var api = {};

	function randomNormal() {
		var u1 = Math.random(), u2 = Math.random();
		var r = Math.sqrt(-2*Math.log(u1));
		var theta = Math.PI*2*u2;
		return r*Math.sin(theta);
	}
	function generateLineSimple(count, gen) {
		gen = gen || randomNormal;
		var p = gen();
		var sqrt2 = Math.sqrt(2);
		var line = [gen(), gen()];
		while (line.length < count) {
			line = splitLine(line, gen);
		}
		return line;
	}
	function splitLine(points, gen) {
		gen = gen || randomNormal;
		var newPoints = [points[0]];
		for (var i = 1; i < points.length; i++) {
			var a = points[i - 1];
			var b = points[i];
			var covar = 1 - 1/(points.length - 1);
			var extra = Math.sqrt(0.5*(1 - covar));
			newPoints.push((a + b)/2 + gen()*extra);
			newPoints.push(b);
		}
		return newPoints;
	}
	function generateLine(period, length, gen) {
		period = Math.ceil(period);
		length = length || period;
		var totalCount = length + period;
		var line = generateLineSimple(totalCount, gen);
		var newLine = [];
		var covar = 1 - period/(line.length - 1);
		var factorA = 1/Math.sqrt(1 - covar*covar), factorB = -covar/Math.sqrt(1 - covar*covar);
		for (var i = 0; i < length; i++) {
			var value = line[i + period]*factorA + line[i]*factorB;
			newLine.push(value);
		}
		return newLine;
	}

	function linePoints(x1, y1, x2, y2, sd, scale) {
		var distance = Math.sqrt((x1 - x2)*(x1 - x2) + (y1 - y2)*(y1 - y2));
		var xVar = generateLine(scale, distance);
		var yVar = generateLine(scale, distance);
		var points = [];
		for (var i = 0; i < xVar.length; i++) {
			var ratio = i/(xVar.length - 1);
			points.push([x1 + (x2 - x1)*ratio + xVar[i]*sd, y1 + (y2 - y1)*ratio + yVar[i]*sd]);
		}
		return points;
	}

	function createCanvas(width, height) {
		var canvas = document.createElement('canvas');
		canvas.width = width;
		canvas.height = height;
		return canvas;
	};

	function blurRow(pixels, y, blurWidth, isOdd) {
		var width = pixels.length, height = pixels[0].length;
		var weight = 0, weights = [0], red = 0, green = 0, blue = 0, redTotal = [0], greenTotal = [0], blueTotal = [0];
		for (var x = 0; x < width; x++) {
			weights.push(++weight);
			redTotal.push(red += pixels[x][y][0]);
			greenTotal.push(green += pixels[x][y][1]);
			blueTotal.push(blue += pixels[x][y][2]);
		}

		var indexWidth = blurWidth*0.5;
		for (var x = 0; x < width; x++) {
			var startIndex = Math.max(0, isOdd ? Math.ceil(x - indexWidth): Math.floor(x - indexWidth));
			var endIndex = Math.min(width, isOdd ? Math.floor(x + 1 + indexWidth) : Math.ceil(x + 1 + indexWidth));
			weight = Math.max(0, weights[endIndex] - weights[startIndex]);
			red = redTotal[endIndex] - redTotal[startIndex];
			green = greenTotal[endIndex] - greenTotal[startIndex];
			blue = blueTotal[endIndex] - blueTotal[startIndex];
			pixels[x][y][0] = red/weight;
			pixels[x][y][1] = green/weight;
			pixels[x][y][2] = blue/weight;
		}
	}

	function blurColumn(pixels, x, blurWidth, isOdd) {
		var width = pixels.length, height = pixels[0].length;
		var weight = 0, weights = [0], red = 0, green = 0, blue = 0, redTotal = [0], greenTotal = [0], blueTotal = [0];
		for (var y = 0; y < height; y++) {
			weights.push(++weight);
			redTotal.push(red += pixels[x][y][0]);
			greenTotal.push(green += pixels[x][y][1]);
			blueTotal.push(blue += pixels[x][y][2]);
		}

		var indexWidth = blurWidth*0.5;
		for (var y = 0; y < height; y++) {
			var startIndex = Math.max(0, isOdd ? Math.ceil(y - indexWidth): Math.floor(y - indexWidth));
			var endIndex = Math.min(height, isOdd ? Math.floor(y + 1 + indexWidth) : Math.ceil(y + 1 + indexWidth));
			weight = Math.max(0, weights[endIndex] - weights[startIndex]);
			red = redTotal[endIndex] - redTotal[startIndex];
			green = greenTotal[endIndex] - greenTotal[startIndex];
			blue = blueTotal[endIndex] - blueTotal[startIndex];
			pixels[x][y][0] = red/weight;
			pixels[x][y][1] = green/weight;
			pixels[x][y][2] = blue/weight;
		}
	}

	api.config = {
		size: 1000,
		resolution: 4,
		timeInterval: 250,
		// Pre-filtering
		medianSize: 0,
		pencilEffect: 5,
		pencilTextureScale: 50,
		// Shading settings
		shadingDensity: 0.8,
		shadingWeight: 0.1,
		shadingThreshhold: 0.3,
		shadingMinLength: 10,
		shadingMaxLength: 20,
		shadingTaper: 0.1,
		shadingSaturation: 1,
		shadingLevels: 16,
		shadingLightness: 1.5,
		shadingEndRandom: 1,
		hueBlurRatio: 0.2,
		hueBlurStages: 5,
		hueCorrectionOffset: 1,
		hueAngle: 0,
		hueAngleRandom: 0.06*Math.PI,
		// Edge settings
		edgeBlurSize: 0.71,
		edgeBlurStages: 5,
		outlineDensity: 1,
		outlineLineLimit: 0.1,
		outlineMinLength: 3,
		outlineThickness: 0.75,
		outlineWeight: 0.15,
		outlineFlatness: 1.5,
		outlineOpacity: 0.65, // final blend
		outlineRepetition: 10
	};

	function Convertor(image, config) {
		this.config = config;

		var eventHandlers = {};
		this.on = function (event, handler) {
			eventHandlers[event] = eventHandlers[event] || [];
			eventHandlers[event].push(handler);
			return this;
		};
		this.emit = function (event) {
			var args = Array.prototype.slice.call(arguments, 1);
			var handlers = eventHandlers[event] || [];
			handlers.forEach(function (handler) {
				handler.apply(this, args);
			}.bind(this));
			return this;
		};

		this._timeout = null;

		var imageArea = image.width*image.height;
		var area = config.size*config.size;
		var scaling = Math.sqrt(area/imageArea);

		this.width = Math.round(image.width*scaling);
		this.height = Math.round(image.height*scaling);

		this._state = {
			stages: ['median', 'blur', 'edge-power', 'blur', 'outline', 'reset-blur', 'hue-blur', 'shading', 'combine']
		};

		this.imageCanvas = createCanvas(this.width, this.height);
		var imageContext = this.imageCanvas.getContext('2d');
		imageContext.fillStyle = '#FFF';
		imageContext.fillRect(0, 0, this.width, this.height);
		imageContext.drawImage(image, 0, 0, this.width, this.height);
	};
	Convertor.prototype = {
		_tick: function () {
			var state = this._state, config = this.config, width = this.width, height = this.height;
			var endTime = Date.now() + config.timeInterval;

			var stage = state.stages[0];
			if (!stage) return this.done();

			// .blurPixels is our "working image"
			if (!this.blurPixels) {
				this.blurCanvas = createCanvas(width, height);
				this.blurPixels = [];
				var originalPixels = this.imageCanvas.getContext('2d').getImageData(0, 0, width, height).data;
				for (var x = 0; x < width; x++) {
					this.blurPixels[x] = [];
					for (y = 0; y < height; y++) {
						var pixelIndex = (x + y*width);
						this.blurPixels[x][y] = [
							originalPixels[pixelIndex*4]/255,
							originalPixels[pixelIndex*4 + 1]/255,
							originalPixels[pixelIndex*4 + 2]/255,
						];
					}
				}
			}
			function pixelsToCanvas(inputPixels, canvas) {
				var context = canvas.getContext('2d');
				var imageData = context.getImageData(0, 0, width, height);
				var pixels = imageData.data;
				for (var x = 0; x < width; x++) {
					for (y = 0; y < height; y++) {
						var pixelIndex = (x + y*width);
						pixels[pixelIndex*4] = Math.min(255, Math.max(0, inputPixels[x][y][0]*255));
						pixels[pixelIndex*4 + 1] = Math.min(255, Math.max(0, inputPixels[x][y][1]*255));
						pixels[pixelIndex*4 + 2] = Math.min(255, Math.max(0, inputPixels[x][y][2]*255));
						pixels[pixelIndex*4 + 3] = 255;
					}
				}
				context.putImageData(imageData, 0, 0);
			}

			// Shading
			if (stage === 'median') {
				this.medianCanvas = this.medianCanvas || createCanvas(width, height);
				if (!state.median) {
					this.oldPixels = this.blurPixels;
					this.blurPixels = this.blurPixels.map(function (column) {
						return column.map(function (pixel) {
							return pixel.slice(0);
						});
					});
				}
				var medianState = state.median = state.median || {
					radius: Math.round(config.medianSize*config.resolution/2),
					row: 0,
					column: 0
				};
				if (medianState.radius < 1) {
					// Skip everything
					state.stages.shift();
					return this.go('median', 1);
				}
				while (Date.now() < endTime && medianState.row < height) {
					var minX = Math.max(0, medianState.column - medianState.radius), maxX = Math.min(width - 1, medianState.column + medianState.radius);
					var minY = Math.max(0, medianState.row - medianState.radius), maxY = Math.min(height - 1, medianState.row + medianState.radius);
					var reds = [], greens = [], blues = [];
					for (var x = minX; x <= maxX; x++) {
						for (var y = minY; y <= maxY; y++) {
							var pixel = this.oldPixels[x][y];
							reds.push(pixel[0]);
							greens.push(pixel[1]);
							blues.push(pixel[2]);
						}
					}
					reds.sort();
					greens.sort();
					blues.sort();
					var index = Math.floor((reds.length + 1)/2 + Math.random());
					this.blurPixels[medianState.column] || (this.blurPixels[medianState.column] = []);
					this.blurPixels[medianState.column][medianState.row] = [reds[index], greens[index], blues[index]];

					medianState.column++;
					if (medianState.column >= width) {
						medianState.column = 0;
						medianState.row++;
					}
				}

				pixelsToCanvas(this.blurPixels, this.medianCanvas);

				if (medianState.row >= height) {
					state.stages.shift();
				}
				this.go('median', medianState.row/height, this.medianCanvas);
			} else if (stage === 'shading') {
				if (!this.shadingCanvas) {
					var shadingLines = width*height/config.resolution/config.resolution*config.shadingDensity;
					state.shadingLines = 0;
					state.shadingLinesTotal = shadingLines;
					this.shadingCanvas = createCanvas(width, height);
				};
				var originalPixels = this.imageCanvas.getContext('2d').getImageData(0, 0, width, height).data;
				var blurPixels = this.blurPixels;

				var context = this.shadingCanvas.getContext('2d');
				while (Date.now() < endTime && state.shadingLines < state.shadingLinesTotal) {
					// Pick point (in vaguely ordered way)
					var blockNumber = Math.floor(state.shadingLines/state.shadingLinesTotal*100);
					var blockX = width/10*Math.floor(blockNumber/10), blockY = height/10*(blockNumber%10);
					var randomX = blockX + Math.random()*width/10 - 0.5, randomY = blockY + Math.random()*height/10 - 0.5;
					var randomX = Math.random()*width - 0.5, randomY = Math.random()*height - 0.5;

					var pixelIndex = Math.round(randomX) + Math.round(randomY)*width;
					var roundX = Math.max(0, Math.min(width - 1, Math.round(randomX)));
					var roundY = Math.max(0, Math.min(height - 1, Math.round(randomY)));

					var red = originalPixels[pixelIndex*4], green = originalPixels[pixelIndex*4 + 1], blue = originalPixels[pixelIndex*4 + 2];
					var blurRed = blurPixels[roundX][roundY][0], blurGreen = blurPixels[roundX][roundY][1], blurBlue = blurPixels[roundX][roundY][2];
					var hueRed = red*(0.5 + config.hueCorrectionOffset)/(blurRed + config.hueCorrectionOffset);
					var hueGreen = green*(0.5 + config.hueCorrectionOffset)/(blurGreen + config.hueCorrectionOffset);
					var hueBlue = blue*(0.5 + config.hueCorrectionOffset)/(blurBlue + config.hueCorrectionOffset);

					var colourDistance = function(x, y) {
						var pixelIndex = Math.round(x) + Math.round(y)*width;
						var red2 = originalPixels[pixelIndex*4] || 0, green2 = originalPixels[pixelIndex*4 + 1] || 0, blue2 = originalPixels[pixelIndex*4 + 2] || 0;
						return Math.sqrt((red - red2)*(red - red2) + (green - green2)*(green - green2) + (blue - blue2)*(blue - blue2))/256;
					};
					// Calculate hue
					if (Math.abs(hueGreen - blue) > 0.01 || Math.abs(2*hueRed - hueGreen - hueBlue) > 0.01) {
						var hue = Math.atan2(Math.sqrt(3)*(hueGreen - blue) , 2*hueRed - hueGreen - hueBlue);
						var maxRgb = Math.max(255 - hueRed, 255 - hueGreen, 255 - hueBlue);
						var minRgb = Math.min(255 - hueRed, 255 - hueGreen, 255 - hueBlue);
						var saturation = (maxRgb - minRgb)/maxRgb;
						if (saturation == 0) {
							hue = Math.random()*Math.PI*2;
						}
					} else {
						var hue = 0;
						var saturation = 0;
					}

					// Calculate angle
					var angle = config.hueAngle + hue + (Math.random() - Math.random())*config.hueAngleRandom;
					if (Math.random() < 0.5) angle += Math.PI;
					var diffX = Math.cos(angle)*config.resolution*0.5, diffY = Math.sin(angle)*config.resolution*0.5;
					var startX = randomX, startY = randomY, endX = randomX, endY = randomY;
					while (startX > 0 && startY > 0 && startX < width && startY < height && colourDistance(startX, startY) < config.shadingThreshhold) {
						startX += diffX;
						startY += diffY;
						var length = Math.sqrt((startX - randomX)*(startX - randomX) + (startY - randomY)*(startY - randomY));
						if (length > config.shadingMaxLength*config.resolution) break;
					}
					while (endX > 0 && endY > 0 && endX < width && endY < height && colourDistance(endX, endY) < config.shadingThreshhold) {
						endX -= diffX;
						endY -= diffY;
						var length = Math.sqrt((endX - randomX)*(endX - randomX) + (endY - randomY)*(endY - randomY));
						if (length > config.shadingMaxLength*config.resolution) break;
					}
					var length = Math.sqrt((endX - startX)*(endX - startX) + (endY - startY)*(endY - startY));
					var alphaModifier = config.shadingMinLength/Math.max(config.shadingMinLength, length/config.resolution);

					var taper = Math.pow(Math.random(), config.shadingTaper);
					var middleX = (startX + endX)*0.5, middleY = (startY + endY)*0.5;
					startX = startX*taper + middleX*(1 - taper);
					startY = startY*taper + middleY*(1 - taper);
					endX = endX*taper + middleX*(1 - taper);
					endY = endY*taper + middleY*(1 - taper);

					var randomAmount = (Math.random()*Math.random()*2)*config.shadingEndRandom*config.resolution;
					var randomAngleStart = Math.random()*Math.PI*2, randomAngleEnd = Math.random()*Math.PI*2;
					startX += randomAmount*Math.cos(randomAngleStart);
					startY += randomAmount*Math.sin(randomAngleStart);
					endX += randomAmount*Math.cos(randomAngleEnd);
					endY += randomAmount*Math.sin(randomAngleEnd);

					// Saturation
					var average = (red + green + blue)/3;
					red = average + (red - average)*config.shadingSaturation;
					green = average + (green - average)*config.shadingSaturation;
					blue = average + (blue - average)*config.shadingSaturation;

					// Quantising
					var quantiseUnit = 255/config.shadingLevels;
					red = Math.round(red/quantiseUnit + Math.random() - 0.5)*quantiseUnit;
					green = Math.round(green/quantiseUnit + Math.random() - 0.5)*quantiseUnit;
					blue = Math.round(blue/quantiseUnit + Math.random() - 0.5)*quantiseUnit;

					// Lightness (gamma-ish)
					var multiplier = Math.min(100, Math.pow(average/255, 1/config.shadingLightness - 1));
					red *= multiplier;
					green *= multiplier;
					blue *= multiplier;

					// Draw stroke
					var strokeStyle = context.strokeStyle = 'rgb(' + [red, green, blue].map(function (c) {
						return Math.max(0, Math.min(255, Math.round(c)));
					}).join(',') + ')';
					//context.globalCompositeOperation = 'darker';
					context.lineCap = 'round';

					var lineWidth = config.resolution/config.pencilEffect;
					var repeats = config.pencilEffect;
					context.globalAlpha = config.shadingWeight*alphaModifier;

					context.lineWidth = lineWidth;
					var targetVariation = config.resolution*config.resolution/6;
					var lineVariation = lineWidth*lineWidth/6;
					var spatialSd = Math.sqrt(targetVariation - lineVariation);
					for (var pencilRepeat = 0; pencilRepeat < repeats; pencilRepeat++) {
						var points = linePoints(startX, startY, endX, endY, spatialSd, config.pencilTextureScale*config.resolution);
						context.beginPath();
						points.forEach(function (pair) {
							context.lineTo(pair[0], pair[1]);
						})
						context.stroke();
					}

					state.shadingLines++;
				}
				if (state.shadingLines >= state.shadingLinesTotal) {
					state.stages.shift();
				}
				return this.go('shading', state.shadingLines/state.shadingLinesTotal, this.shadingCanvas);
			} else if (stage === 'reset-blur') {
				this.blurPixels = this.blurCanvas = null; // reset
				state.stages.shift();
				return this.go('resetting', 1, this.blurCanvas);
			} else if (stage === 'blur' || stage === 'hue-blur') {
				if (!state.boxBlurDirection) {
					var blurSize = config.resolution*config.edgeBlurSize, blurStages = config.edgeBlurStages;
					if (stage === 'hue-blur') {
						blurSize = config.hueBlurRatio*config.size;
						blurStages = config.hueBlurStages;
					}
					var boxBlurVariance = blurSize*blurSize/blurStages; // desired variance
					var boxBlurWidth = 1;
					while ((boxBlurWidth + 1)*(boxBlurWidth + 1) - 1 < boxBlurVariance) {
						boxBlurWidth++;
					}
					boxBlurVariance = (boxBlurWidth + 1)*(boxBlurWidth + 1) - 1; // actual variance
					state.boxBlurStage = 0;
					state.boxBlurStages = Math.ceil(blurSize*blurSize/boxBlurVariance);
					state.boxBlurWidth = boxBlurWidth;
					state.boxBlurDirection = 'row';
					state.boxBlurPos = 0;
					return this.go('blurring', 0, this.blurCanvas);
				}
				while (Date.now() < endTime && state.boxBlurStage < state.boxBlurStages) {
					if (state.boxBlurDirection == 'row') {
						blurRow(this.blurPixels, state.boxBlurPos, state.boxBlurWidth, state.boxBlurStage%2);

						state.boxBlurPos++;
						if (state.boxBlurPos >= height) {
							state.boxBlurDirection = 'column';
							state.boxBlurPos = 0;
						}
					} else {
						blurColumn(this.blurPixels, state.boxBlurPos, state.boxBlurWidth, state.boxBlurStage%2);

						state.boxBlurPos++;
						if (state.boxBlurPos >= width) {
							state.boxBlurDirection = 'row';
							state.boxBlurPos = 0;
							state.boxBlurStage++;
						}
					}
				}

				// Preview
				pixelsToCanvas(this.blurPixels, this.blurCanvas);

				if (state.boxBlurStage >= state.boxBlurStages) {
					state.stages.shift();
					state.boxBlurDirection = null;
				}

				var stageProgress = (state.boxBlurDirection === 'column') ? (state.boxBlurPos/width*0.5 + 0.5) : (state.boxBlurPos/height*0.5);
				this.go('blurring', (state.boxBlurStage + stageProgress)/state.boxBlurStages, this.blurCanvas);
			} else if (stage === 'save-reference') {
				// Make a copy
				this.referencePixels = this.blurPixels.map(function (row) {
					return row.map(function (pixel) {
						return pixel.slice(0);
					})
				});
				state.stages.shift();

				this.go('storing as reference', 1, this.blurCanvas);
			} else if (stage === 'edge-power') {
				var edgePixels = [], blurPixels = this.blurPixels, maxSd = 0;
				var originalPixels = this.imageCanvas.getContext('2d').getImageData(0, 0, width, height).data;
				for (var x = 0; x < width; x++) {
					edgePixels[x] = [];
					for (y = 0; y < height; y++) {
						if (this.referencePixels) {
							var red = this.referencePixels[x][y][0], green = this.referencePixels[x][y][1], blue = this.referencePixels[x][y][2];
						} else {
							var pixelIndex = (x + y*width);
							var red = originalPixels[pixelIndex*4]/255, green = originalPixels[pixelIndex*4 + 1]/255, blue = originalPixels[pixelIndex*4 + 2]/255;
						}
						var sd = Math.sqrt((this.blurPixels[x][y][0] - red)*(this.blurPixels[x][y][0] - red) + (this.blurPixels[x][y][1] - green)*(this.blurPixels[x][y][1] - green) + (this.blurPixels[x][y][2] - blue)*(this.blurPixels[x][y][2] - blue));
						edgePixels[x][y] = sd;
						maxSd = Math.max(maxSd, sd);
					}
				}

				for (var x = 0; x < width; x++) {
					for (y = 0; y < height; y++) {
						var sd = Math.pow(1 - edgePixels[x][y]/maxSd, config.outlineFlatness);
						blurPixels[x][y] = [sd, sd, sd];
					}
				}

				state.stages.shift();

				this.go('calculating edges', 1, this.blurCanvas);
			} else if (stage === 'outline') {
				if (!this.outlineCanvas) {
					this.outlineCanvas = createCanvas(width, height);
					this.edgeCanvas = createCanvas(width, height);

					pixelsToCanvas(this.blurPixels, this.edgeCanvas);

					state.outlineLength = 0;
					state.outlineLengthLimit = width*height/config.resolution/config.resolution*config.outlineDensity;
				}
				var edgeContext = this.edgeCanvas.getContext('2d');
				var outlineContext = this.outlineCanvas.getContext('2d');
				var globalMinValue = 255;
				while (Date.now() < endTime && state.outlineLength < state.outlineLengthLimit) {
					var edgeImageData = edgeContext.getImageData(0, 0, width, height);
					var edgePixels = edgeImageData.data;
					// Find minimum value
					var minX = 0, minY = 0, minValue = 255;
					for (var x = 0; x < width; x++) {
						if (x < config.resolution || x + config.resolution > width) continue;
						for (var y = 0; y < height; y++) {
							if (y < config.resolution || y + config.resolution > height) continue;
							var pixelIndex = (x + y*width);
							var value = edgePixels[pixelIndex*4]; // red channel
							if (value < minValue) {
								minX = x;
								minY = y;
								minValue = value;
							}
						}
					}
					globalMinValue = Math.min(globalMinValue, minValue);

					function getValue(x, y) {
						x = Math.min(width - 1, Math.max(0, Math.round(x)));
						y = Math.min(height - 1, Math.max(0, Math.round(y)));
						var pixelIndex = (x + y*width);
						return edgePixels[pixelIndex*4]; // red channel
					}

					function bestAngle(x, y, minAngle, maxAngle, distance, steps) {
						var bestAngle = minAngle, bestValue = 255;
						for (var i = 0; i < steps; i++) {
							var angle = minAngle + (maxAngle - minAngle)*i/(steps - 1);
							var possibleX = x + Math.cos(angle)*distance;
							var possibleY = y + Math.sin(angle)*distance;
							var value = getValue(possibleX, possibleY); // red channel
							if (value < bestValue) {
								bestAngle = angle;
								bestValue = value;
							}
						}
						return bestAngle;
					}

					var points = [];
					var angle = bestAngle(minX, minY, 0, Math.PI, config.resolution, 20);
					var startAngle = angle;
					var stepDistance = config.resolution;
					var cornerRate = Math.PI*0.25, cornerLimit = Math.PI*0.5;
					var minAngle = angle - cornerLimit, maxAngle = angle + cornerLimit;
					var bestValue = getValue(minX, minY), valueLimit = 255 - config.outlineLineLimit*(255 - bestValue);
					// Push front forward
					var frontX = minX, frontY = minY;
					while (getValue(frontX, frontY) < valueLimit && frontX > 0 && frontX < width && frontY > 0 && frontY < height) {
						points.unshift([frontX, frontY]);
						angle = bestAngle(frontX, frontY, angle - cornerRate, angle + cornerRate, stepDistance, 10);
						if (angle < minAngle || angle > maxAngle) {
							break;
						}
						frontX += stepDistance*Math.cos(angle);
						frontY += stepDistance*Math.sin(angle);
					}
					// Extend backwards
					angle = startAngle + Math.PI;
					var backX = minX, backY = minY;
					points.pop(); // Remove the minX/minY entry, as it's going to be re-added in a bit
					while (getValue(backX, backY) < valueLimit && backX > 0 && backX < width && backY > 0 && backY < height) {
						points.push([backX, backY]);
						angle = bestAngle(backX, backY, angle - cornerRate, angle + cornerRate, stepDistance, 10);
						if (angle < minAngle || angle > maxAngle) {
							break;
						}
						backX += stepDistance*Math.cos(angle);
						backY += stepDistance*Math.sin(angle);
					}

					// Erase line from edge image
					if (Math.random() < 0.5) {
						edgeContext.strokeStyle = 'rgb(255, 0, ' + Math.round(Math.random()*255) + ')';
					} else {
						edgeContext.strokeStyle = 'rgb(255, ' + Math.round(Math.random()*128) + ', 0)';
					}
					edgeContext.lineWidth = config.resolution;
					edgeContext.lineCap = 'round';
					edgeContext.globalAlpha = 1/config.outlineRepetition;
					edgeContext.beginPath();
					if (points.length > 1) {
						points.forEach(function (p) {
							edgeContext.lineTo(p[0], p[1]);
						});
					} else {
						edgeContext.arc(points[0][0], points[0][1], 0.1, 0, Math.PI);
					}
					edgeContext.stroke();

					// Draw line on image
					if (points.length >= config.outlineMinLength) {
						outlineContext.strokeStyle = '#000';
						var targetThickness = config.outlineThickness*config.resolution;
						var lineWidth = outlineContext.lineWidth = targetThickness/config.pencilEffect;
						console.log('lineWidth outline', lineWidth);

						var targetVariation = targetThickness*targetThickness/6;
						var lineVariation = lineWidth*lineWidth/6;
						var spatialSd = Math.sqrt(targetVariation - lineVariation);

						outlineContext.lineCap = 'round';
						var strength = (255 - minValue)/(255 - globalMinValue);
						outlineContext.globalAlpha = config.outlineWeight*strength;
						for (var i = 0; i < config.pencilEffect; i++) {
							var offsetX = generateLine(config.pencilTextureScale*config.resolution/stepDistance, points.length), offsetY = generateLine(points.length);
							var wiggleAmount = spatialSd;
							outlineContext.beginPath();
							points.forEach(function (p, i) {
								outlineContext.lineTo(p[0] + offsetX[i]*wiggleAmount, p[1] + offsetY[i]*wiggleAmount);
							});
							outlineContext.stroke();
						}
					}

					state.outlineLength += stepDistance*points.length;
					break;
				}

				if (state.outlineLength >= state.outlineLengthLimit) {
					state.stages.shift();
				}

				this.go('outline', state.outlineLength/state.outlineLengthLimit, this.outlineCanvas);
			} else if (stage === 'combine') {
				this.resultCanvas = createCanvas(width, height);
				var context = this.resultCanvas.getContext('2d');
				context.fillStyle = '#FFF';
				context.fillRect(0, 0, width, height);
				if (this.shadingCanvas) context.drawImage(this.shadingCanvas, 0, 0);
				context.globalAlpha = config.outlineOpacity;
				if (this.outlineCanvas) context.drawImage(this.outlineCanvas, 0, 0);
				state.stages.shift();
				this.go('combine', 1, this.resultCanvas);
			} else {
				throw new Error('Unknown stage: ' + stage);
			}
		},
		go: function (stage, progress, canvas) {
			this._timeout = setTimeout(this._tick.bind(this), 0);
			return this.emit('progress', stage || 'start', progress || 0, canvas || this.imageCanvas);
		},
		done: function () {
			this.stop();
			this.go = function () {
				throw new Error('Cannot re-start finished image');
			};
			this.emit('progress', 'done', 1, this.resultCanvas);
			this.emit('done', this.resultCanvas);
		},
		stop: function () {
			clearTimeout(this._timeout);
			this._timeout = false;
			return this.emit('stop');
		}
	};

	api.convertImage = function (image, config, progressCallback, doneCallback) {
		if (typeof config !== 'object') {
			doneCallback = progressCallback;
			progressCallback = config;
			config = null;
		}
		config = Object.create(config || {});
		for (var key in api.config) {
			if (!(key in config)) config[key] = api.config[key];
		}
		if (!doneCallback) {
			doneCallback = progressCallback;
			progressCallback = null;
		}

		var convertor = new Convertor(image, config);
		if (doneCallback) convertor.on('done', doneCallback);
		if (progressCallback) convertor.on('progress', progressCallback);
		return convertor.go();
	};

	return api;
})();
