# sketchify-browser

This is a version of my [sketchify](https://bitbucket.org/geraintluff/sketchify) algorithm that works in the browser.

This has only been properly tested on Chrome.  If you encounter issues on other (sane, modern) browsers, please do let me know.

It is slow, so you've got to have some patience - and please note that (at least on Chrome) it works faster when it's visible on the screen, due to browser optimisations.

### Known issues

For large images, saving images from `<canvas>` elements is broken (can crash the browser).  Data URLs produced by the canvas are also too long, and crash the browser if used with `window.open()`.  The current solution is to translate the data URL into a Blob/Object URL, and open that in a new window.

However, Object URLs require a domain to work (which exclused `file://` URLs).  This means that although the *processing* works if you open it as a local file, saving the results as a PNG only works if you serve it over HTTP.

## Example

![example output](https://bytebucket.org/geraintluff/sketchify-browser/raw/master/example-output.jpg)
![example original](https://bytebucket.org/geraintluff/sketchify-browser/raw/master/example-input.jpg)